orcl_stmt = "insert into scott.obj_type(id,name) values(1,'2');insert into scott.obj_type(id,name) values(1,'4');"
parsed_orcl_stmt = sqlparse.parse(orcl_stmt)
formatted_tokens = []

if str(parsed_orcl_stmt[0].tokens[0]).lower() == 'insert':
	i=0
	for token in parsed_orcl_stmt[0].tokens:
		i=i+1
		if i==4: 
			insert_dt = self.timestampValue				
			formatted_tokens.append(str(token[i]).replace('(', 'partition(insert_dt="'+insert_dt[0:10]+'") ('))
		else:
			formatted_tokens.append(str(token).replace('"', '')) if not str(token).startswith("values (") else formatted_tokens.append(str(token))
elif str(parsed_orcl_stmt[0].tokens[0]).lower() == 'delete' or str(
		parsed_orcl_stmt[0].tokens[0]).lower() == 'update':
	for token in parsed_orcl_stmt[0].tokens:
		if not str(token).startswith('where'):
			formatted_tokens.append(str(token).replace('"', ''))
		else:
			formatted_where_clause = []
			split_token = ''
			if '\' and "' in str(token):
				split_where_clause = str(token).split(' and "')
				split_token = ' and '
			elif '\' or "' in str(token):
				split_where_clause = str(token).split(' or "')
				split_token = ' or '
			else:
				split_where_clause = [str(token)]
			for item in split_where_clause:
				if '" =' in item:
					split_item = item.split('" =')
					formatted_where_clause.append(
						"{0}={1}".format(split_item[0].replace('"', ''), split_item[1]))
				elif '"=' in item:
					split_item = item.split('"=')
					formatted_where_clause.append(
						"{0}={1}".format(split_item[0].replace('" =', ''), split_item[1]))
				else:
					formatted_where_clause.append(item)
			formatted_tokens.append(split_token.join(formatted_where_clause))
            print(formatted_tokens)			